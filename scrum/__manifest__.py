{
    'name': 'Risk Type',  # model name for resarch
    'version': '15.0',
    'summary': 'this model contains the type of possible risks',
    'sequence': '1',  # id in the database
    'category': 'project',
    'depends': ['base', 'hr'],
    'data': [
        'security/scrum_security.xml',
        'security/ir.model.access.csv',
        'views/risk_type_views.xml',
        'views/menu.xml',
        'views/hr_employee_views.xml',
        'report/risk_type_report.xml',
        'views/risk_type_templates.xml'
    ],
    'installable': True,
    'application': True,

}
