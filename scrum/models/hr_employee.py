from odoo import models, fields, api, _


class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    code = fields.Char('code')
    exp_ids = fields.One2many('risk.type', 'employee_id', string="exp")
