from odoo import models, fields, _
from odoo.odoo15.odoo import api
from odoo.odoo15.odoo.exceptions import ValidationError


class RiskType(models.Model):
    _name = 'risk.type'  # nom du model
    # ----------------------------------------------------------#
    # model's fields
    # ----------------------------------------------------------#
    name = fields.Char(string="Name", required=True)
    active = fields.Boolean(default=1)
    company_id = fields.Many2one('res.company', string="Company" , default=lambda self: self.env.company)
    state = fields.Selection([('draft', 'Draft'),
                              ('in_progress', 'In_progress'),
                              ('closed', 'CLosed')], string="State", default='draft')
    number = fields.Integer(default=1, string="number.risks")  # valeur par defaut
    code = fields.Char(string="code")  # string c'est le label
    job_id = fields.Many2one("hr.job", string="job")
    employee_id = fields.Many2one("hr.employee", string="Employee")
    department_id = fields.Many2one("hr.department", string="department")  # on a relier employee avec le departm
    amount = fields.Float(string="Amount")
    amount_tax = fields.Float(string="Amount_tax")
    total = fields.Float(string="total", compute="_compute_total",
                         store=True)  # compute indique que le champ sera calculer à partir des autres champs et store pour le creer en base de donnner

    # ----------------------------------------------------------------------------------------
    # fields bussiness methode
    # -----------------------------------------------------------------------------------------
    def set_inprogress(self):
        if self.state == 'draft':
            self.state = 'inprogress'

    def set_draft(self):
        if self.state == 'closed':
            self.state = 'draft'

    def set_closed(self):
        if self.state == "inprogress":
            self.state = "closed"

    @api.constrains('code')  # on va tester sur le code
    def _check_code(self):
        """check code"""
        for risktype in self:  # risk type est un variable
            if risktype.code and not risktype.code.isdigit():  # make sur digit type
                raise ValidationError(_('code invalid'))  # tiret bas pour la traduction aprés

    @api.onchange('employee_id')  # on va tester sur employee
    def _onchange_employee_id(self):
        if self.employee_id:
            self.job_id = self.employee_id.job_id.id  # how to get job id of user id

    @api.depends('amount', 'amount_tax')  # la resultat depend de 2 fields
    def _compute_total(self):
        for record in self:
            record.total = record.amount + record.amount_tax

    @api.model
    def create(self, vals):
        record = super(RiskType, self).create(vals)  # disasterType is the class name
        record.code = '1'  # mettre par defautle code en 1 lors de creation
        return record
